//Récuperer les diffèrents elements
var sidebar = document.getElementById("side-navbar");
var sidebar_elt = sidebar.getElementsByTagName("span");
var dropdown = document.getElementById("dropdown-btn");
var caret = document.getElementById("caret");

//Ajout d'un listner pour l'evenement mouseenter pour la barre de navigation
sidebar.addEventListener('mouseenter', e => {  //callback (fonction qui s'execute si l'evenement est déclenché)
    //si la largeur dépasse 960 px
    if (document.documentElement.clientWidth > 960) {
        sidebar.style.width = '200px';
        // parcourir les elements textes de la navbar et les affichier sur la meme ligne que le logo
        for(i = 0; i<sidebar_elt.length;i++){
            sidebar_elt[i].style.display = 'inline';
            sidebar_elt[i].style.fontSize = ' 20px';
        }
        // recupere l'element apres le dropdown et ses "children" et on les affiche
        for(i = 0; i<dropdown.nextElementSibling.children.length;i++){
            dropdown.nextElementSibling.children[i].style.display = 'block';
            dropdown.nextElementSibling.children[i].style.fontSize = ' 20px';
        }
    }
});

//Ajout d'un listner pour l'evenement mouseleave pour la barre de navigation
sidebar.addEventListener('mouseleave', e => {
    if (document.documentElement.clientWidth > 900) {
        sidebar.style.width = '80px';
        // parcourir les elements textes de la navbar et les masquer
        for (i = 0; i < sidebar_elt.length; i++) {
            sidebar_elt[i].style.display = 'none';
        }
        for (i = 0; i < dropdown.nextElementSibling.children.length; i++) {
            dropdown.nextElementSibling.children[i].style.display = 'none'
        }
    }
});


function ShowSubElements() {
    // toggle voit si la classe présente est active et la desactive ou sinon la reactive
    dropdown.classList.toggle("active");
    var dropdownContent = dropdown.nextElementSibling;
    // Changer le sens de la fléche epres le clic
    if (caret.className === 'fa fa-caret-right'){
        caret.className = 'fa fa-caret-down'
    } else {
        caret.className = 'fa fa-caret-right'
    }
    //Afficher les sous titres si ils etaient masqué sinon les masqué
    if (dropdownContent.style.display === "block") {
        dropdownContent.style.display = "none";
    } else {
        dropdownContent.style.display = "block";
    }

}

//Utile pour les formats réduits (conditions en css)
//Bouton qui affiche et masque la navbar
function displayNavbar() {
    sidebar.style.display = 'block';
}
function hideNavbar() {
    sidebar.style.display = 'none';
}
